﻿namespace Shopster.Entities;

public interface IEntity
{
    Guid Id { get; set; }
}